<?php

namespace App\Controller;

use App\Repository\ReservationRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController'
        ]);
    }

    /**
     * @Route("/admin/stats", name="admin_stats")
     */
    public function statistics(ReservationRepository $reservationRepository)
    {
        $time = new Datetime();
        $week = new Datetime();
        $week->modify('+7 day');

        return $this->render('admin/stats.html.twig', [
            'reservations_today' => $reservationRepository->stats_today(date_format($time, 'Y-m-d 00:00:00'), date_format($time, 'Y-m-d 23:59:59')),
            'reservations_week' => $reservationRepository->stats_week(date_format($time, 'Y-m-d 00:00:00'), date_format($week, 'Y-m-d 23:59:59')),
        ]);
    }
}
