<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Entity\Room;
use App\Form\BookType;
use App\Form\SearchRoomType;
use App\Repository\ReservationRepository;
use App\Repository\RoomRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class DefaultController extends AbstractController
{

    /**
     * @Route("/", name="default")
     */
    public function index(RoomRepository $roomRepository,Request $request) : Response
    {
        $form = $this->createForm(SearchRoomType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $startdate = date_format($data['start_date'], 'Y-m-d');
            $enddate = date_format($data['end_date'], 'Y-m-d');
            $occupants = $data['occupants'];

            return $this->redirectToRoute('result_name', ['startdate' => $startdate, 'enddate' => $enddate, 'occupants' => $occupants]);
        }
        return $this->render('default/search.html.twig', [
            'controller_name' => 'DefaultController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/result/startdate={startdate}&enddate={enddate}&occupants={occupants}", name="result_name")
     */
    public function search($startdate, $enddate, $occupants, RoomRepository $roomRepository, Request $request)  : Response{

        return $this->render('default/search_result.html.twig', [
            'rooms' => $roomRepository->occupiedFinder($startdate, $enddate, $occupants),
            'startdate' => $startdate,
            'enddate' => $enddate,
            'occupants' => $occupants
        ]);
    }

    /**
     * @Route("/result/startdate={startdate}&enddate={enddate}&occupants={occupants}/overview/{id}", name="overview")
     */
    public function overview($startdate, $enddate, $occupants, RoomRepository $roomRepository, Request $request, $id)  : Response{
        $form = $this->createForm(BookType::class);
        $form->handleRequest($request);
        $total_days = date_diff(date_create($startdate), date_create($enddate))->format("%d");
        $room = $this->getDoctrine()->getRepository(Room::class)->find($id);
        $total_price = $total_days * $room->getPrice();

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$this->security->isGranted('ROLE_USER')) {

                return $this->render('default/overview.html.twig', [
                    'form' => $form->createView(),
                    'id' => $id,
                    'rooms' => $roomRepository->findBy(['id' => $id]),
                    'total_days' => $total_days,
                    'startdate' => $startdate,
                    'enddate' => $enddate,
                    'occupants' => $occupants,
                    'total_price' => $total_price,
                    'loggedin' => 0
                ]);

            } else{
                return $this->redirectToRoute('book', [
                    'id' => $id,
                    'rooms' => $roomRepository->findBy(['id' => $id]),
                    'total_days' => $total_days,
                    'startdate' => $startdate,
                    'enddate' => $enddate,
                    'occupants' => $occupants,
                    'total_price' => $total_price,
                ]);
            }
        }


        return $this->render('default/overview.html.twig', [
            'form' => $form->createView(),
            'rooms' => $roomRepository->findBy(['id' => $id]),
            'total_days' => date_diff(date_create($startdate), date_create($enddate))->format("%d"),
            'startdate' => $startdate,
            'enddate' => $enddate,
            'occupants' => $occupants,
            'loggedin' => 1
        ]);
    }

    /**
     * @Route("/result/startdate={startdate}&enddate={enddate}&occupants={occupants}/overview/{id}/book/price={total_price}", name="book")
     */
    public function book($startdate, $enddate, $occupants, $id, $total_price, RoomRepository $roomRepository)
    {
        {
            $format = 'Y-m-d';
            $room = $this->getDoctrine()->getRepository(Room::class)->find($id);
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $reservation = new Reservation();
            $reservation->setStartDate(DateTime::createFromFormat($format, $startdate));
            $reservation->setEndDate(DateTime::createFromFormat($format, $enddate));
            $reservation->setRoomId($room);
            $reservation->setUserId($user);
            $reservation->setPrice($total_price);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($reservation);
            $entityManager->flush();

            return $this->redirectToRoute('your_bookings', [
                'new' => 1,
            ]);
        }
    }
    /**
     * @Route("/bookings", name="your_bookings")
     */
    public function yourBookings(ReservationRepository $reservationRepository){

        return $this->render('default/your_bookings.html.twig', [
           'reservations' => $reservationRepository->findBy(['user_id' => $this->get('security.token_storage')->getToken()->getUser()]),
        ]);
    }
}
