<?php

namespace App\Repository;

use App\Entity\Reservation;
use App\Entity\Room;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @method Room|null find($id, $lockMode = null, $lockVersion = null)
 * @method Room|null findOneBy(array $criteria, array $orderBy = null)
 * @method Room[]    findAll()
 * @method Room[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Room::class);
    }

    // /**
    //  * @return Room[] Returns an array of Room objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Room
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function occupiedFinder($begindate, $enddate, $occupants)
    {
        $query = $this->_em->createQueryBuilder();

        $subquery = $this->_em->createQueryBuilder()
            ->select('identity(reservation.room_id)')
            ->from(Reservation::class, 'reservation')
            ->where('reservation.start_date BETWEEN :from AND :to')
            ->orWhere('reservation.end_date BETWEEN :from AND :to')
            ->orWhere(':from BETWEEN reservation.start_date and reservation.end_date ')
            ->orderBy('reservation.start_date', 'ASC')
            ->setParameters(['from' => $begindate, 'to' => $enddate])
            ->getDQL();


        return $query->select('room')
            ->from(Room::class, 'room')
            ->where($query->expr()->notIn('room.id', $subquery))
            ->andWhere(':occupants = room.occupants')
            ->getQuery()
            ->setParameters(['from' => $begindate, 'to' => $enddate, 'occupants' => $occupants])->getResult();
    }
}


