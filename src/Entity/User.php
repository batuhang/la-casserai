<?php
// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tel_nr;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $mobile_nr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true))
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $insertion_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true))
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true))
     */
    private $zip;

    /**
     * @ORM\Column(type="string", length=255, nullable=true))
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true))
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisation")
     */
    private $organisation_id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $last_activity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true))
     */
    private $function;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation", mappedBy="user_id")
     */
    private $reservations;

    public function __construct()
    {
        parent::__construct();
        $this->reservations = new ArrayCollection();
    }

    public function getTelNr(): ?int
    {
        return $this->tel_nr;
    }

    public function setTelNr(?int $tel_nr): self
    {
        $this->tel_nr = $tel_nr;

        return $this;
    }

    public function getMobileNr(): ?int
    {
        return $this->mobile_nr;
    }

    public function setMobileNr(?int $mobile_nr): self
    {
        $this->mobile_nr = $mobile_nr;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getInsertionName(): ?string
    {
        return $this->insertion_name;
    }

    public function setInsertionName(?string $insertion_name): self
    {
        $this->insertion_name = $insertion_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(?string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection|Organisation[]
     */
    public function getOrganisationId(): Collection
    {
        return $this->organisation_id;
    }

    public function addOrganisationId(Organisation $organisationId): self
    {
        if (!$this->organisation_id->contains($organisationId)) {
            $this->organisation_id[] = $organisationId;
            $organisationId->setFunction($this);
        }

        return $this;
    }

    public function removeOrganisationId(Organisation $organisationId): self
    {
        if ($this->organisation_id->contains($organisationId)) {
            $this->organisation_id->removeElement($organisationId);
            // set the owning side to null (unless already changed)
            if ($organisationId->getFunction() === $this) {
                $organisationId->setFunction(null);
            }
        }

        return $this;
    }

    public function getLastActivity(): ?\DateTimeInterface
    {
        return $this->last_activity;
    }

    public function setLastActivity(?\DateTimeInterface $last_activity): self
    {
        $this->last_activity = $last_activity;

        return $this;
    }

    public function getFunction(): ?string
    {
        return $this->function;
    }

    public function setFunction(string $function): self
    {
        $this->function = $function;

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setUserId($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->contains($reservation)) {
            $this->reservations->removeElement($reservation);
            // set the owning side to null (unless already changed)
            if ($reservation->getUserId() === $this) {
                $reservation->setUserId(null);
            }
        }

        return $this;
    }
}